
typedef struct listNode
{
	unsigned int num;
	struct listNode* next;
} listNode;


listNode* addNode(listNode* head, listNode* addThis);
listNode* removeNode(listNode* head);
listNode* createNode(int num);
void printList(listNode* head);
